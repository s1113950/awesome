import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.File;
import java.io.FileNotFoundException;

public class Initializer {
	
	public ArrayList<String> phrases = new ArrayList<String>();	//stores phrases people enter
	public HashMap<String, ArrayList<String>> wordRelations = new HashMap<String, ArrayList<String>>();
	//word entered is key, values are in an arraylist of whole phrases that contain the word
	public HashMap<String, Double> weights = new HashMap<String, Double>();	//contains weights for what string to output
	//string in this case is each string in the arraylist of word relations
	public boolean firstTime, saidHello = false;

	public Initializer() {
		readFiles();
	}

	private void readFiles() {
		File file = new File("phrases.txt");
		Scanner scanner;	//scanner for each word in line
		String line, word, response;	//response is for corresponding to wordrelations
		double weight = 0.0;	//init weight variable

		try {	//if file exists or not
			Scanner textFile = new Scanner(file);
			firstTime = false;
			
			while(textFile.hasNextLine()) {	//loop through each line
				line = textFile.nextLine();
				scanner = new Scanner(line);
				scanner.useDelimiter(" ");

				phrases.add(line);	//add whole line to arraylist of phrases, possibly delete later?

				while(scanner.hasNext()) {	//loop through each word in said line
					word = scanner.next();
					try {
						wordRelations.get(word).add(line);
					} catch(Exception e) {
						ArrayList<String> firstList = new ArrayList<String>();
						firstList.add(line);
						wordRelations.put(word, firstList);
					}
				}
			}
		} catch(FileNotFoundException e) {	//if no data file, first time user
			//System.err.println("FileNotFoundException: " + e.getMessage());
			System.out.println("Welcome first time user! :)");
			//firstTime = true;
		}

		file = new File("weights.txt");
		try {	//get weights, order is (double) weight, word, responses separated by a (***)
			Scanner textFile = new Scanner(file);
			firstTime = false;
			
			while(textFile.hasNextLine()) {
				line = textFile.nextLine();
				scanner = new Scanner(line);
				scanner.useDelimiter(" ");

				weight = scanner.nextDouble();	//get weight

				scanner.useDelimiter("\\z");	//get til end of input
				response = scanner.next();
				weights.put(response, weight);	//assign weights for each response
			} 
		} catch(FileNotFoundException e) {
			firstTime = true;
		}
	}
}