import java.util.Scanner;
import java.io.FileNotFoundException;

public class Main {
	public static void main(String args[]) throws FileNotFoundException {
		Scanner scan = new Scanner(System.in);
		String input;
		String[] exitWords = {"goodbye", "bye", "done", "exit", "leave", "adios"}, inputWords;
		boolean keepGoing = true;
		StringManip stringManip = new StringManip();
		Initializer initialize = new Initializer();
		Analyzer analyze = new Analyzer();

		System.out.println("What can I do for you?");

		while(keepGoing) {
			input = scan.nextLine();	//get input
			inputWords = input.split(" ");	//split into words

			if(stringManip.checkIfContains(input, exitWords)) {	//check if an exit word is used, terminate program, else choose output
				if(inputWords.length == 1) {
					System.out.println("Goodbye");
					keepGoing = false;
				}
			} else {
				analyze.chooseOutput(input, initialize);
			}
		}
	}
}