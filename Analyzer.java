import java.util.ArrayList;
import java.util.Map;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Analyzer {
	public void chooseOutput(String input, Initializer data) {
		String output;

		storeInput(input, data);	//store input user enters
		output = selectOutput(input, data);	//pick what to output
		saveData(data);	//once input stored and output selected, save data

		System.out.println(output);
	}

	private void storeInput(String input, Initializer data) {
		if(!data.phrases.contains(input)) {//try and get the existing input string, means that it already exists, don't store duplicates
			data.phrases.add(input);	///if input not stored in phrases yet, add input string to arraylist of input strings
			data.weights.put(input, 0.1);	//intialize input line to 0.1 weight of want to output
		}

		String[] inputWords = input.split(" ");
		for(String word : inputWords) {	//loop through all words in the input string
			try {	//try and add to the arraylist
				data.wordRelations.get(word).add(input); 
			} catch(Exception e) {	//if no arraylist initialized for the input string yet
				ArrayList<String> firstList = new ArrayList<String>();
				firstList.add(input);
				data.wordRelations.put(word, firstList);
			}
		}
	}

	private String selectOutput(String input, Initializer data) {
		String choice = data.phrases.get(0);
		StringManip stringManip = new StringManip();
		String checkHelloWords = {"hello", "good morning"};

		if(data.firstTime) {	//if first time running program
			data.firstTime = false;
			if(!input.toLowerCase().contains("hello")) {	//if the person didn't say hello :(
				return("Aww you didn't say hello to me...:(");
			} else {	//if the person did say hello :)
				data.saidHello = true;	//boolean to see if the person said hello or not
				return("Hello! :)");
			}
		} else {	//if not first time through
			if(input.toLowerCase().contains("hello") && data.saidHello == false) {	//if they said hello for first time
				return("Well hello to you to too :)");
			} else if(input.toLowerCase().contains("hello") && data.saidHello == true) { //if they already said hello
				return("We already exchanged greetings, but hello again!");
			}
		}

		/*
		for(String line : data.phrases) {
			if(data.weights.get(line) > data.weights.get(choice))
				choice = line;
		}
		*/
		return(choice);
	}

	private void saveData(Initializer data) {
		File phrasesFile = new File("phrases.txt");
		File weightsFile = new File("weights.txt");
		BufferedWriter output;
		
		phrasesFile.delete();	//remove the old phrases file
		weightsFile.delete();	//remove old phrases file

		File outputFile = new File("phrases.txt");
		try {
			output = new BufferedWriter(new FileWriter(outputFile));	//try and write to output
			for(String line : data.phrases) {	//output each line to the file
				output.write(line + "\n");
			}
			output.close();
		} catch(IOException e) {
			e.printStackTrace();
		}

		outputFile = new File("weights.txt");
		try {
			output = new BufferedWriter(new FileWriter(outputFile));
			for(Map.Entry<String, Double> entry : data.weights.entrySet()) {
				output.write(entry.getValue() + " " + entry.getKey() + "\n");
			}
			output.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}