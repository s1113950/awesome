public class StringManip {
	public boolean checkIfContains(String input, String wordsToCheck[]) {
		String[] inputWords = input.split(" ");
		boolean contains = false;

		for(String word : inputWords) {	//loop through input string words
			for(String checkWord : wordsToCheck) {	//loop through string array of words to check
				if(checkWord.toLowerCase().equals(word.toLowerCase()))
					contains = true;
			}
		}
		return contains;
	}
}